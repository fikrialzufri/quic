@extends('template.app')

@section('content')

<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar {{$title}}</h3>
          <a href="{{route($route.'.create')}}" class="btn btn-sm btn-primary float-right text-light">
            <i class="fa fa-plus"></i>Tambah Data
          </a>
          <a href="{{route($route.'.formupload')}}" class="btn btn-sm btn-danger float-right text-light">
            <i class="fa fa-plus"></i> Upload
          </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <form action="" role="form" id="form" enctype="multipart/form-data">
            <div class="row">

              <div class="col-lg-3">
                <label for="nama">Nama</label>
                <div class="input-group">
                  <input type="text" name="nama" class="form-control" id=" nama" value="{{$nama}}">
                </div>

              </div>
              <div class="col-lg-3">
                <label for="kecamatan">Kecamatan </label>
                <div class="input-group">
                  <select name="kecamatan" class="selected2 custom-select" id="cmbkecamatan">
                    <option value="">--Pilih Kecamatan--</option>
                    @foreach ($dataKecamatan as $kecamatans)
                    <option value="{{$kecamatans->id}}" {{$kecamatan == $kecamatans->id ? "selected" : ""}}>{{'Kecamatan : '.$kecamatans->nama}}</option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="col-lg-3">
                <label for="">Aksi</label>
                <div class="input-group">


                  <button type="submit" class="btn btn-warning">
                    <span class="fa fa-search"></span>
                    Cari
                  </button>
                </div>
              </div>
            </div>
          </form>
          <table class="table table-bordered  table-responsive">
            <thead>
              <tr>
                <th>No</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>Kabupaten</th>
                <th class="text-center" width="20%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($dataKelurahan as $index => $item)

              <tr>
                <td>{{ $index+1+(($dataKelurahan->CurrentPage()-1)*$dataKelurahan->PerPage()) }}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->nama_kecamatan}}</td>
                <td>{{$item->nama_kabupaten}}</td>
                <td class="text-center">
                  <a href="{{route($route.'.edit',$item->id)}}" class="btn btn-sm btn-warning text-light">
                    <i class="nav-icon fas fa-edit"></i> Ubah</a>
                  <form id="form-{{$item->id}}" action="{{ route($route.'.destroy', $item->id)}}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                  </form>
                  <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick=deleteconf("{{$item->id}}")>
                    <i class="fa fa-trash"></i> Hapus
                  </button>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="10">Data {{$title}} tidak ada</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $dataKelurahan->appends(request()->input())->links() }}
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->

  @stop

  @push('script')
  <script>
    $('#cmbkecamatan').select2({
      placeholder: '--- Pilih Kecamatan---',
      width: '100%'
    });
  </script>
  @endpush