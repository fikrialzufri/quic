@extends('template.app')

@section('content')

<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar {{$title}}</h3>

          <form id="form-123" action="{{ route('perhitungan.hapus')}}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
          <!-- <button class="btn btn-danger float-right btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick=deleteconf("123")>
            <i class="fa fa-trash"></i> Hapus Perhitungan
          </button> -->
          <a href="{{route($route.'.create')}}" class="btn btn-sm btn-primary float-right text-light">
            <i class="fa fa-plus"></i>Tambah Data
          </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <form action="" role="form" id="form" enctype="multipart/form-data">
            <div class="row">

              <div class="col-lg-2">
                <label for="paslon">Paslon</label>
                <div class="input-group">
                  <select name="paslon" class="selected2 custom-select" id="cmbpaslon">
                    <option value="">--Pilih Paslon--</option>
                    <option value="all" {{$paslon == "all" ? "selected" : ""}}>Semua</option>
                    @foreach ($dataPaslon as $Paslon)
                    <option value="{{$Paslon->id}}" {{$paslon == $Paslon->id ? "selected" : ""}}>{{'Paslon : '.$Paslon->nama.', No Urut :'.$Paslon->nourut}}</option>
                    @endforeach
                  </select>
                </div>

              </div>
              <div class="col-lg-3">
                <label for="">Kecamatan dan Kelurahan</label>
                <select name="kelurahan" class="selected2 custom-select" id="cmbkelurahan">
                  <option value="">--Pilih Kelurahan--</option>
                  <option value="allkecamatan" {{$kelurahan == "allkecamatan" ? "selected" : ""}}>Semua Kecamatan & Kelurahan</option>
                  @foreach ($dataKelurahan as $item)
                  <option value="{{$item->id}}" {{$kelurahan == $item->id ? "selected" : ""}}>{{'Kecamatan : '.$item->nama_kecamatan.', Kelurahan : '.$item->nama}}</option>
                  @endforeach
                  @foreach ($dataKecamatan as $kecamatan)
                  <option value="{{$kecamatan->id}}" {{$kelurahan == $kecamatan->id ? "selected" : ""}}>{{'Kecamatan : '.$kecamatan->nama}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-2">
                <label for="tps">TPS</label>
                <div class="input-group">
                  <input type="text" name="tps" class="form-control" id="tps" value="{{$tps}}">
                </div>
              </div>

              <div class="col-lg-4">
                <label for="">Aksi</label>
                <div class="input-group">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-warning">
                      <span class="fa fa-search"></span>
                      Cari
                    </button>
                    <div class="input-group-append">
                      <a href="{{route('perhitungan.excel')}}?paslon={{$paslon}}&kelurahan={{$kelurahan}}&tps={{$tps}}" class="btn btn-success" target="_blank">
                        <span class="fas fa-file-excel"></span>
                        Download Excel
                      </a>
                    </div>
                  </div>
                </div>
              </div>
          </form>
          <table class="table table-bordered table-responsive">
            <thead>
              <tr>
                <th>No</th>
                <th>No Urut</th>
                <th>Paslon</th>
                <th>Saksi</th>
                <th>TPS</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
                <th>Foto C1</th>

              </tr>
            </thead>
            <tbody>
              @forelse ($dataPerhitungan as $index => $item)

              <tr>
                <td>{{ $index+1+(($dataPerhitungan->CurrentPage()-1)*$dataPerhitungan->PerPage()) }}</td>
                <td>{{$item->nourut}}</td>
                <td>{{$item->nama_paslon}}</td>
                <td>{{$item->nama_saksi}}</td>
                <td>{{$item->nama_tps}}</td>
                <td>{{$item->nama_kelurahan}}</td>
                <td>{{$item->nama_kecamatan}}</td>
                <td>{{tanggal_indonesia($item->tanggal)}}</td>
                <td>{{$item->jumlah}}</td>
                <td>
                  @if($item->fotocsatu)
                  <a href=" {{ asset('storage/saksi/'.$item->fotocsatu)}}" download>Foto C1</a>
                  @else
                  belum ada
                  @endif
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="10">Data Perhitungan tidak ada</td>
              </tr>
              @endforelse
              <tr>
                <td colspan="7"></td>
                <td><b> Total</b></td>
                <td><b>{{$jumlah}}</b></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $dataPerhitungan->appends(request()->input())->links() }}
        </div>
      </div>
      <!-- ./col -->
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Suara</h3>

        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered table-responsive ">
            <thead>
              <tr>
                <th>No</th>
                <th>Suara</th>
                <th>Saksi</th>
                <th>TPS</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($dataPerhitunganNonPaslon as $index => $item)

              <tr>
                <td>{{ $index+1 }}</td>
                <td>{{$item->nama_paslon}}</td>
                <td>{{$item->nama_saksi}}</td>
                <td>{{$item->nama_tps}}</td>
                <td>{{$item->nama_kelurahan}}</td>
                <td>{{$item->nama_kecamatan}}</td>
                <td>{{tanggal_indonesia($item->tanggal)}}</td>
                <td>{{$item->jumlah}}</td>

              </tr>
              @empty
              <tr>
                <td colspan="9">Data Suara tidak ada</td>
              </tr>
              @endforelse
              <tr>
                <td colspan="6"></td>
                <td><b> Total</b></td>
                <td><b>{{$jumlahNon}}</b></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $dataPerhitunganNonPaslon->appends(request()->input())->links() }}
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->

  @stop

  @push('script')
  <script>
    $('#cmbpaslon').select2({
      placeholder: '--- Pilih Paslon---',
      width: '100%'
    });
    $('#cmbkelurahan').select2({
      placeholder: '--- Pilih Kecamatan---',
      width: '100%'
    });
    $('#cmbkecamatan').select2({
      placeholder: '--- Pilih Kecamatan---',
      width: '100%'
    });
    $('#cmbtps').select2({
      placeholder: '--- Pilih TPS---',
      width: '100%'
    });
  </script>
  @endpush