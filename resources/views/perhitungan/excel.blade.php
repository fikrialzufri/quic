<h3>Hasil Perhitungan Paslon</h3>

<table style="border: 1px solid black;">
    <thead>
        <tr>
            <th style="border: 3px solid black;">No</th>
            <th style="border: 3px solid black;">No Urut</th>
            <th style="border: 3px solid black;">Paslon</th>
            <th style="border: 3px solid black;">Saksi</th>
            <th style="border: 3px solid black;">TPS</th>
            <th style="border: 3px solid black;">Keluarahan</th>
            <th style="border: 3px solid black;">Kecamatan</th>
            <th style="border: 3px solid black;"> Tanggal</th>
            <th style="border: 3px solid black;">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($dataPerhitungan as $index => $item)

        <tr>
            <td style="border: 3px solid black;">{{ $index+1 }}</td>
            <td style="border: 3px solid black;">{{$item->nourut}}</td>
            <td style="border: 3px solid black;">{{$item->nama_paslon}}</td>
            <td style="border: 3px solid black;">{{$item->nama_saksi}}</td>
            <td style="border: 3px solid black;">{{$item->nama_tps}}</td>
            <td style="border: 3px solid black;">{{$item->nama_kelurahan}}</td>
            <td style="border: 3px solid black;">{{$item->nama_kecamatan}}</td>
            <td style="border: 3px solid black;">{{tanggal_indonesia($item->tanggal)}}</td>
            <td style="border: 3px solid black;">{{$item->jumlah}}</td>
        </tr>
        @empty
        <tr>
            <td colspan="10">Data {{$title}} tidak ada</td>
        </tr>
        @endforelse
        <tr style="border: 3px solid black;">
            <td colspan="7" style="border: 3px solid black;"></td>
            <td style="border: 3px solid black;"><b> Total</b></td>
            <td style="border: 3px solid black;"><b>{{$jumlah}}</b></td>
        </tr>
    </tbody>
    <tfoot>

    </tfoot>
</table>

<h3>Hasil Suara Sah dan Tidak Sah</h3>
<table style="border: 1px solid black;">
    <thead>
        <tr>
            <th style="border: 3px solid black;">No</th>
            <th style="border: 3px solid black;">Suara</th>
            <th style="border: 3px solid black;">Saksi</th>
            <th style="border: 3px solid black;">TPS</th>
            <th style="border: 3px solid black;">Keluarahan</th>
            <th style="border: 3px solid black;">Kecamatan</th>
            <th style="border: 3px solid black;"> Tanggal</th>
            <th style="border: 3px solid black;">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($dataPerhitunganNonPaslon as $index => $item)

        <tr>
            <td style="border: 3px solid black;">{{ $index+1 }}</td>
            <td style="border: 3px solid black;">{{$item->nama_paslon}}</td>
            <td style="border: 3px solid black;">{{$item->nama_saksi}}</td>
            <td style="border: 3px solid black;">{{$item->nama_tps}}</td>
            <td style="border: 3px solid black;">{{$item->nama_kelurahan}}</td>
            <td style="border: 3px solid black;">{{$item->nama_kecamatan}}</td>
            <td style="border: 3px solid black;">{{tanggal_indonesia($item->tanggal)}}</td>
            <td style="border: 3px solid black;">{{$item->jumlah}}</td>
        </tr>
        @empty
        <tr>
            <td colspan="9">Data Suara tidak ada</td>
        </tr>
        @endforelse
        <tr style="border: 3px solid black;">
            <td colspan="6" style="border: 3px solid black;"></td>
            <td style="border: 3px solid black;"><b> Total</b></td>
            <td style="border: 3px solid black;"><b>{{$jumlahNon}}</b></td>
        </tr>
    </tbody>
    <tfoot>

    </tfoot>
</table>