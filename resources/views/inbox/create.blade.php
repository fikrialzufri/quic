@extends('template.app')

@section('content')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <!-- /.card-header -->
                <form method="post" role="form" enctype="multipart/form-data" id="sample_form">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                <label for="pesan" class=" form-control-label">Isi Pesan</label>
                            </div>
                            <div>
                                <textarea class="form-control {{$errors->has('pesan') ? 'form-control is-invalid' : 'form-control'}}" rows="3" placeholder="Tulis pesan anda ..." name="pesan" id="pesan"></textarea>
                            </div>
                            <span class="text-danger" id="errorPesan">
                                <strong id="textpesan"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div>
                                <label for="semua" class=" form-control-label" id="semua">Kirim Semua Saksi</label>
                            </div>
                            <div class="form-check">
                                <input name="semua" class="form-check-input" type="checkbox" id="checksemua">
                                <label class="form-check-label">Semua Saksi</label>
                            </div>
                        </div>
                        <div class="form-group " id="saksiinput">
                            <label for="saksi">Saksi</label>
                            <select name="saksi[]" class="selected2 form-control " id="cmbsaksi" multiple="multiple">
                                <option value="">--Pilih saksi--</option>
                                @foreach ($dataSaksi as $saksi)
                                <option value="{{$saksi->id}}" {{old('saksi') == $saksi->id ? "selected" : ""}}>{{'saksi : '.$saksi->nama.', Kabupaten :'.$saksi->nama_kabupaten.', TPS :'.$saksi->nama_tps}}</option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="errorSaksi">
                                <strong id="textsaksi"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="save" id="save" class="btn btn-info" value="Simpan" />
                        </div>

                        <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                        <h4 class="modal-title">Mohon Tunggu Proses SMS</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="">
                                            </div>
                                        </div>
                                        <h5 id="number"></h5>
                                        <!-- <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">

                                        </div> -->
                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                    </div>
                                    <!-- <div class="modal-body"> -->
                                    <!-- <div class="form-group" id="process" style="display:none;">
                                            
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="">
                                                </div>
                                            </div>
                                        </div> -->
                                    <!-- </div> -->
                                </div>

                            </div>
                        </div>
                </form>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

    @stop

    @push('style')
    <style>
        .select2-selection--multiple {
            overflow: hidden !important;
            height: auto !important;
        }
    </style>
    @endpush
    @push('script')
    <script>
        $(function() {
            // $("#pesan").on("keydown", function(e) {
            //     // $("#pesan").removeClass("is-invalid");
            //     $("#textpesan").text("");
            //     console.log("ad");
            //     console.log(e);
            // });

            $('textarea').on('keyup', function(e) {
                $("#textpesan").text("");
            })
            $('#cmbsaksi').select2({
                placeholder: '--- Pilih Saksi---',
                width: '100%'
            });

            $('#checksemua').change(function() {
                if (this.checked)
                    $('#saksiinput').fadeOut('slow');
                else
                    $('#saksiinput').fadeIn('slow');


            });
            $('#sample_form').on('submit', function(event) {
                event.preventDefault();
                var count_error = 0;

                if ($('#pesan').val() == '') {
                    $('#textpesan').text('Harap isi pesan dengan benar');
                    count_error++;
                } else {
                    $('#textpesan').text('');
                }

                if (!$('#checksemua').is(":checked")) {
                    // it is checked
                    console.log($('#cmbsaksi').val());
                    if ($('#cmbsaksi').val() == '') {
                        $('#textsaksi').text('Harap isi saksi dengan benar');
                        count_error++;
                    } else {
                        $('#textsaksi').text('');
                    }
                }



                if (count_error == 0) {
                    $.ajax({
                        url: "{{ $action }}",
                        method: "POST",
                        data: $(this).serialize(),
                        beforeSend: function() {
                            $('#save').attr('disabled', 'disabled');
                            $('#process').css('display', 'block');
                        },
                        success: function(data) {
                            console.log(data)
                            var percentage = 0;
                            $('#myModal').modal('toggle');
                            var timer = setInterval(function() {
                                percentage = percentage + 20;
                                $('#number').text("Proses : " + percentage + "%");
                                progress_bar_process(percentage, timer);
                                if (percentage >= 100) {
                                    pageRedirect();
                                }
                            }, 1000);

                        }
                    })
                } else {
                    return false;
                }
            });

            function pageRedirect() {
                window.location.href = "{{route('inbox.index')}}";
            }

            function progress_bar_process(percentage, timer) {
                $('.progress-bar').css('width', percentage + '%');
                if (percentage > 100) {
                    clearInterval(timer);
                    $('#sample_form')[0].reset();
                    $('#process').css('display', 'none');
                    $('.progress-bar').css('width', '0%');
                    $('#save').attr('disabled', false);
                    $('#success_message').html("<div class='alert alert-success'>Data Saved</div>");
                    setTimeout(function() {
                        $('#success_message').html('');
                    }, 5000);
                }
            }

        });
    </script>
    @endpush