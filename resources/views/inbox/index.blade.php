@extends('template.app')

@section('content')

<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar {{$title}}</h3>
          <a href="{{route(strtolower($route).'.create')}}" class="btn btn-sm btn-primary float-right text-light">
            <i class="fa fa-plus"></i>Tambah SMS
          </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <form action="" role="form" id="form" enctype="multipart/form-data">
            <div class="row">

              <div class="col-lg-3">
                <label for="jenis">Jenis SMS</label>
                <div class="input-group">
                  <select name="jenis" class="selected2 custom-select" id="cmbjenis">
                    <option value="">--Pilih Jenis--</option>
                    <option value="inbox" {{$jenis == "inbox" ? "selected" : ""}}>Pesan Masuk</option>
                    <option value="outbox" {{$jenis == "outbox" ? "selected" : ""}}>Pesan Keluar</option>
                    <option value="send" {{$jenis == "send" ? "selected" : ""}}>Pesan Terkirim</option>
                  </select>
                </div>

              </div>

              <div class="col-lg-3">
                <label for="">Aksi</label>
                <div class="input-group">


                  <button type="submit" class="btn btn-warning">
                    <span class="fa fa-search"></span>
                    Cari
                  </button>
                </div>
              </div>
            </div>
          </form>
          <table class="table table-bordered  table-hover table-responsive" id="table">
            <thead>
              <tr>
                <th>No</th>
                <th>Pengirim</th>
                <th>Content</th>
                <th>Waktu</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Jenis</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($dataInbox as $index => $item)

              <tr role="button" data-href="{{route('inbox.show', $item->id)}}">
                <td>{{ $index+1+(($dataInbox->CurrentPage()-1)*$dataInbox->PerPage()) }}</td>
                <td>{{$item->sender}}</td>
                <td>{{$item->content}}</td>
                <td>{{$item->time}}</td>
                <td>{{tanggal_indonesia($item->created_at)}}</td>
                <td>
                  @if($item->status == 'tidak')<span class="badge bg-danger">Belum dibaca</span>
                  @else <span class="badge bg-primary">Sudah Dibaca</span>
                  @endif</td>
                <td>
                  @if($item->jenis == 'outbox')
                  <span class="badge bg-danger">Belum Terkirim</span>
                  @elseif($item->jenis == 'send')
                  <span class="badge bg-warning">Terkirim</span>
                  @else
                  <span class="badge bg-success">Inbox</span>
                  @endif</td>
              </tr>
              @empty
              <tr>
                <td colspan="10">Data {{$title}} tidak ada</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $dataInbox->appends(request()->input())->links() }}
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->

  @stop

  @push('style')
  <style>
    [data-href] {
      cursor: pointer;
    }
  </style>
  @endpush
  @push('script')
  <script>
    $(".table").on("click", "tr[role=\"button\"]", function(e) {
      window.location = $(this).data("href");
    });
    $('#cmbjenis').select2({
      placeholder: '--- Pilih Jenis---',
      width: '100%'
    });
  </script>
  @endpush