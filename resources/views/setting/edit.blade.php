@extends('template.app')

@section('content')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <!-- /.card-header -->
                <form action="{{ $action }}" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                <label for="nama" class=" form-control-label">Setting Polling Saksi</label>
                            </div>
                            <div>
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" name="aktif" id="customSwitch3" {{$setting->aktif == "ya" ? 'checked' : ""}}>
                                    <label class="custom-control-label" for="customSwitch3">Aktif (Hijau) / Tidak Aktif (Merah)</label>
                                </div>
                            </div>
                            @if ($errors->has('nama'))
                            <span class="text-danger">
                                <strong id="textnama">{{ $errors->first('nama')}}</strong>
                            </span>
                            @endif
                        </div>


                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

    @stop

    @push('script')
    <script>
        $(function() {
            $("#nama").keypress(function() {
                $("#nama").removeClass("is-invalid");
                $("#textnama").html("");
            });

        });
    </script>
    @endpush