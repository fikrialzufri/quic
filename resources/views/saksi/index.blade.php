@extends('template.app')

@section('content')

<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar {{$title}}</h3>
          <a href="{{route($route.'.create')}}" class="btn btn-sm btn-primary float-right text-light">
            <i class="fa fa-plus"></i>Tambah Data
          </a>
          <a href="{{route($route.'.formupload')}}" class="btn btn-sm btn-danger float-right text-light">
            <i class="fa fa-plus"></i> Upload
          </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <form action="" role="form" id="form" enctype="multipart/form-data">
            <div class="row">

              <div class="col-lg-2">
                <label for="nama">Nama</label>
                <div class="input-group">
                  <input type="text" name="nama" class="form-control" id=" nama" value="{{$nama}}">
                </div>

              </div>
              <div class="col-lg-2">
                <label for="hp">No Hp</label>
                <div class="input-group">
                  <input type="text" name="hp" class="form-control" id=" hp" value="{{$nohp}}">
                </div>

              </div>
              <div class="col-lg-2">
                <label for="paslon">Status </label>
                <div class="input-group">
                  <select name="status" class="selected2 custom-select" id="cmbstatus">
                    <option value="">--Pilih Status Kirim--</option>
                    <option value="semua" {{$status == "semua" ? "selected" : ""}}>Semua</option>
                    <option value="sudah" {{$status == "sudah" ? "selected" : ""}}>Mengirim</option>
                    <option value="belum" {{$status == "belum" ? "selected" : ""}}>Belum Mengirim</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-3">
                <label for="">Kecamatan dan Kelurahan</label>
                <select name="kelurahan" class="selected2 custom-select" id="cmbkelurahan">
                  <option value="">--Pilih Kelurahan--</option>
                  <option value="allkecamatan" {{$kelurahan == "allkecamatan" ? "selected" : ""}}>Semua Kecamatan & Kelurahan</option>
                  @foreach ($dataKelurahan as $item)
                  <option value="{{$item->id}}" {{$kelurahan == $item->id ? "selected" : ""}}>{{'Kecamatan : '.$item->nama_kecamatan.', Kelurahan : '.$item->nama}}</option>
                  @endforeach
                  @foreach ($dataKecamatan as $kecamatan)
                  <option value="{{$kecamatan->id}}" {{$kelurahan == $kecamatan->id ? "selected" : ""}}>{{'Kecamatan : '.$kecamatan->nama}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-lg-3">
                <label for="">Aksi</label>
                <div class="input-group">


                  <button type="submit" class="btn btn-warning">
                    <span class="fa fa-search"></span>
                    Cari
                  </button>
                </div>
              </div>
            </div>
          </form>
          <br>
          <table class="table table-bordered table-responsive">
            <thead>
              <tr>
                <th>No</th>
                <th>NIK</th>
                <th>No Hp</th>
                <th>Nama</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>Kabupaten</th>
                <th>TPS</th>
                <th>Jumlah</th>
                <th>Status</th>
                <th class="text-center" width="20%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($dataSaksi as $index => $item)

              <tr>
                <td>{{ $index+1+(($dataSaksi->CurrentPage()-1)*$dataSaksi->PerPage()) }}</td>
                <td>{{$item->nik}}</td>
                <td>{{$item->nohp}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->nama_kelurahan}} </td>
                <td>{{$item->nama_kecamatan}} </td>
                <td>{{$item->nama_kabupaten}} </td>
                <td>{{$item->nama_tps}} </td>
                <td>
                  @forelse($item->perhitungan as $value)
                  {{ $loop->first ? '' : ', ' }}
                  No Urut {{$value->nourut}} Jumlah {{$value->jumlah}}
                  @empty
                  @endforelse
                </td>
                <td>
                  @forelse($item->perhitungan as $key => $value)
                  @if ($key == 0)
                  <span class="badge bg-primary">Sudah Mengirim</span>
                  @endif
                  @empty
                  <span class="badge bg-danger">Belum Mengirim</span>
                  @endforelse
                </td>
                <td class="text-center">
                  <a href="{{route($route.'.edit',$item->id)}}" class="btn btn-sm btn-warning text-light">
                    <i class="nav-icon fas fa-edit"></i> Ubah</a>
                  <form id="form-{{$item->id}}" action="{{ route($route.'.destroy', $item->id)}}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                  </form>
                  <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick=deleteconf("{{$item->id}}")>
                    <i class="fa fa-trash"></i> Hapus
                  </button>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="11">Data {{$title}} tidak ada</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $dataSaksi->appends(request()->input())->links() }}
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->

  @stop

  @push('script')
  <script>
    $('#cmbkelurahan').select2({
      placeholder: '--- Pilih Kecamatan---',
      width: '100%'
    });
    $('#cmbstatus').select2({
      placeholder: '--- Pilih Status---',
      width: '100%'
    });
  </script>
  @endpush