@extends('template.app')

@section('content')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Upload {{$title}}</h3>
                </div>
                <!-- /.card-header -->
                <form action="{{ $action }}" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                <label for="nama" class=" form-control-label">Silahkan upload file {{$title}} dibawah ini</label>
                            </div>
                            <div>
                                <input type="file" name="file" id="exampleInputFile" required>
                            </div>
                            @if ($errors->has('file'))
                            <span class="text-danger">Mohon upload dengan benar, file harus berekstensi .csv dengan format yang sesuai.</span>
                            @endif
                            <span class="text-danger">
                                <a href="{{asset('excel/Template '.$title.'.xlsx')}}">Download Template {{$title}}</a>
                            </span>
                        </div>

                    </div>

                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

    @stop

    @push('script')
    <script>
        $(function() {
            $("#nama").keypress(function() {
                $("#nama").removeClass("is-invalid");
                $("#textNama").html("");
            });
            $("#nik").keypress(function() {
                $("#nik").removeClass("is-invalid");
                $("#textnik").html("");
            });
            $("#nik").inputFilter(function(value) {
                return /^\d*$/.test(value); // Allow digits only, using a RegExp
            });
            $("#nohp").inputFilter(function(value) {
                return /^\d*$/.test(value); // Allow digits only, using a RegExp
            });
            $("#nohp").keypress(function() {
                $("#nohp").removeClass("is-invalid");
                $("#textnohp").html("");
            });
            $("#alamat").keypress(function() {
                $("#alamat").removeClass("is-invalid");
                $("#textalamat").html("");
            });
            $('#cmbtps').select2({
                placeholder: '--- Pilih TPS---',
                width: '100%'
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#icon").change(function() {
                readURL(this);
                $('.img-responsive').remove();
            });

            $('.close').on('click', function() {
                $('#image').remove();
            });
        });
    </script>
    @endpush