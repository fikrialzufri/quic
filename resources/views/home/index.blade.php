@extends('template.app')

@section('content')

<div class="container-fluid" id="homeDasboardh">
  @if (!$saksi)
  <!-- Small boxes (Stat box) -->
  <div class="row">

    <div class="col-md-8">

      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title"> Chart Paslon</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="chart">
            <canvas id="perda" height="160"></canvas>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
    <div class="col-md-4">

      <div class="card card-warning">
        <div class="card-header">
          <h3 class="card-title"> Chart Suara</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="chart">
            <canvas id="suara" height="351"></canvas>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  <div class="row">

    <!-- small box -->
    @foreach($paslon as $value)
    <div class="col-md-3">
      <div class="card card-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-success">
          <h5 style="color:black">No Urut : {{$value->nourut}}</h5>
          <h3 style="color:black">{{$value->nama}}</h3>
        </div>
        <div class="widget-user-image">
          <img class="img-circle elevation-2" width="20%" @if ($value->foto == NULL )
          src="{{asset('img/default-icon.png')}}"
          @else
          src="{{ asset('storage/paslon/thumbnail/'.$value->foto)}}"
          @endif>
        </div>
        <div class="card-footer">
          <div class="row">

            <!-- /.col -->
            <div class="col-sm-12">
              <div class="description-block">
                <h4 class="description-header">Jumlah Suara</h4>
                <h2 class="description-text" id="suara-{{$value->nourut}}">{{$value->suara}}</h2>
                <h4 class="description-header">Presentasi</h4>
                @if($value->suara)
                <h2 class="description-text" id="presentasicalon-{{$value->nourut}}">{{round(($value->suara / $seluruh) * 100, 2)}} %</h2>
                @endif
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
    </div>
    @endforeach
    <!-- ./col -->
    <!-- ./col -->

    <!-- ./col -->
    <!-- 
      <div class="col-lg-3 col-6">
        <!-- small box 
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>65</h3>

            <p>Unique Visitors</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div> 
      -->
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <!-- Main row -->

  <!-- /.row (main row) -->
  @else
  @if($setting->aktif == 'ya')
  <form action="{{ route('perhitungan.saksi')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
      @foreach($paslonall as $key => $value)
      <div class="col-md-3">

        <div class="info-box">
          <span class="info-box-icon bg-success">
            <img class="img-circle elevation-2" width="100%" @if ($value->foto == NULL )
            src="{{asset('img/default-icon.png')}}"
            @else
            src="{{ asset('storage/paslon/thumbnail/'.$value->foto)}}"
            @endif>
          </span>

          <div class="info-box-content">
            <span class="info-box-text">{{$value->nama}}</span>
            <span class="info-box-number">
              <input type="text" name="hasil[{{$key}}]" placeholder="" id="hasil[{{$value->id}}]" class="form-control hasil {{$errors->has('hasil') ? 'form-control is-invalid' : 'form-control'}}" value="{{$value->suara_saksi}}" required>

              <input type="hidden" name="id[{{$key}}]" value="{{$value->id}}">
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      @endforeach
      <div class="col-md-12 col-xs-12">
        <div class="form-group">
          <label for="foto" class="control-label">Upload C1</label>
          <input type="file" value="foto" name="foto" id="icon" class="form-control" value="{{old('foto')}}">
        </div>
        <div class="preview"></div>
        @if ($saksi->fotocsatu == NULL )
        <img class="img-profile img-responsive" width="80%" src="{{ asset('storage/saksi/'.$saksi->fotocsatu)}}">
        @endif
        <div>
          <br>
          @if ($errors->has('foto'))
          <span class="text-danger">Mohon isi foto, upload foto dengan benar. File harus berekstensi JPG, PNG, BMP atau JPEG {{ $errors->first('foto')}}</span>
          @endif
        </div>
        <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
      </div>
      <!-- small box -->
    </div>
  </form>
  @else
  <h1>Polling Sudah ditutup !</h1>
  @endif
  @endif

  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
      <div class="modal-content" style="width: 48px">
        <span class="fa fa-spinner fa-spin fa-3x"></span>
      </div>
    </div>
  </div>
</div><!-- /.container-fluid -->

@stop

@push('chart')
@if (!$saksi)
<script>
  var nourut = @json($nourut);
  var suara = @json($suara);
  // console.log(suara);
  $(document).ready(function() {
    function respondCanvas(label, suara) {

    }

    var GetChartData = function() {

      var opt = {

        events: false,
        tooltips: {
          enabled: false,
          fontSize: 40
        },
        hover: {
          animationDuration: 0
        },

        responsive: true,
        legend: {
          display: true,
          labels: {
            padding: 3,
            fontSize: 25
          },
          position: 'bottom',

        },

        animation: {
          duration: 1,
          onComplete: function() {
            var chartInstance = this.chart,
              ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(25, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            this.data.datasets.forEach(function(dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function(bar, index) {
                var data = dataset.data[index];
                ctx.fillText(data, bar._model.x, bar._model.y - 0.3);
              });
            });
          }
        },


        scales: {
          yAxes: [{
            margin: 100,
            ticks: {
              suggestedMin: 0,
              suggestedMax: true,

            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 18
            }
          }]
        }
      };

      $.ajax({
        url: "{{route('perhitungan.hasil')}}",
        method: "GET",
        success: function(data) {
          // var hasil = data.data;
          var label = [];
          var suara = [];
          var labelnonsah = [];
          var suaranonsah = [];
          $.each(data.nourut, function(index, value) {
            var presentasicalon = data.presentasicalon[index];
            label.push("No Urut " + value);

          });
          $.each(data.suara, function(index, item) {
            suara.push(item);
            var nourut = data.nourut[index];
            $('#suara-' + nourut).html(item);
          });

          $.each(data.presentasicalon, function(index, item) {
            var nourut = data.nourut[index];
            $('#presentasicalon-' + nourut).html(item + "%");
          });

          labelnonsah.push("Suara Sah");
          labelnonsah.push("Tidak Sah");
          labelnonsah.push("Seluruh");

          suaranonsah.push(data.suarasah);
          suaranonsah.push(data.suaratidak);
          suaranonsah.push(data.seluruh);

          var ctx = document.getElementById('perda').getContext('2d');
          var chart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: label,
              datasets: [{
                label: 'Jumlah Suara Paslon',
                backgroundColor: '#00a81c',
                borderColor: 'rgb(255, 255, 255)',
                data: suara
              }]
            },

            options: opt
          });

          var ctc = document.getElementById('suara').getContext('2d');
          var chartsuara = new Chart(ctc, {
            type: 'bar',
            data: {
              labels: labelnonsah,
              datasets: [{
                label: 'Presentasi | ' + data.persetase + "%",
                backgroundColor: '#fdbb2d',
                borderColor: 'rgb(255, 255, 255)',
                data: suaranonsah
              }]
            },
            options: opt
          });
        }
      });
    };
    // var GetChartDataSuara = function() {

    //   var opt = {
    //     events: false,
    //     tooltips: {
    //       enabled: false
    //     },
    //     hover: {
    //       animationDuration: 0
    //     },
    //     animation: {
    //       duration: 1,
    //       onComplete: function() {
    //         var chartInstance = this.chart,
    //           ctx = chartInstance.ctx;
    //         ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
    //         ctx.textAlign = 'center';
    //         ctx.textBaseline = 'bottom';

    //         this.data.datasets.forEach(function(dataset, i) {
    //           var meta = chartInstance.controller.getDatasetMeta(i);
    //           meta.data.forEach(function(bar, index) {
    //             var data = dataset.data[index];
    //             ctx.fillText(data, bar._model.x, bar._model.y - 5);
    //           });
    //         });
    //       }
    //     }
    //   };
    // };

    GetChartData();
    setInterval(function() {
      GetChartData();
    }, 6000);

  })
</script>
@else
<script>
  var type = "{{ Session::get('swall') }}";
  // console.log(type);
  const swalWithBootstrapButtons = swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })
  switch (type) {
    case 'oke':
      swalWithBootstrapButtons(
        'Sukses!',
        'Terimakasih, anda telah melakukan polling',
        'success'
      )
      break;
  }
</script>
@endif
@push('style')
<style>
  @media (max-width: 500px) {
    #perda {
      height: 52px;
    }
  }

  .modal {
    text-align: center;
  }

  @media screen and (min-width: 768px) {
    .modal:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      position: absolute;
      height: 100%;

    }
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    top: 50%;
  }
</style>
@endpush
@push('script')
@endpush
<script>
  $(function() {
    $(".hasil").inputFilter(function(value) {
      return /^\d*$/.test(value); // Allow digits only, using a RegExp
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#icon").change(function() {
      readURL(this);
      $('.img-responsive').remove();
    });

    $('.close').on('click', function() {
      $('#image').remove();
    });
    $('#simpan').on('click', function() {
      $('#myModal').modal('show');
      setTimeout(function() {
        console.log("maaf file anda terlalu besar");
        // wait a minute after you recieved the data
      }, 60000)
    });


  });
</script>
<!-- <script src="{{asset('template/plugins/chart.js/Chart.min.js')}}"></script> -->

@endpush