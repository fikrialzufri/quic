@extends('template.app')

@section('content')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Ubah {{$title}}</h3>
                </div>
                <!-- /.card-header -->
                <form action="{{ $action }}" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                <label for="kode" class=" form-control-label">Kode {{$title}}</label>
                            </div>
                            <div>
                                <input type="text" name="kode" placeholder="Kode {{$title}}" id="kode" class="form-control  {{$errors->has('kode') ? 'form-control is-invalid' : 'form-control'}}" value="{{$setup->kode}}" required>
                            </div>
                            @if ($errors->has('kode'))
                            <span class="text-danger">
                                <strong id="textKode">{{ $errors->first('kode')}}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div>
                                <label for="pesan" class=" form-control-label">pesan {{$title}}</label>
                            </div>
                            <div>
                                <input type="text" name="pesan" placeholder="pesan {{$title}}" id="pesan" class="form-control  {{$errors->has('pesan') ? 'form-control is-invalid' : 'form-control'}}" value="{{$setup->pesan}}" required>
                            </div>
                            @if ($errors->has('pesan'))
                            <span class="text-danger">
                                <strong id="textpesan">{{ $errors->first('pesan')}}</strong>
                            </span>
                            @endif
                        </div>


                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

    @stop

    @push('script')
    <script>
        $(function() {
            $("#pesan").keypress(function() {
                $("#pesan").removeClass("is-invalid");
                $("#textpesan").html("");
            });
            $("#kode").keypress(function() {
                $("#kode").removeClass("is-invalid");
                $("#textKode").html("");
            });

            $("#icon").change(function() {
                readURL(this);
                $('.img-responsive').remove();
            });

            $('.close').on('click', function() {
                $('#image').remove();
            });


            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $(".preview").html("<img src='" + e.target.result + "' width='310' id='image'>");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#icon").change(function() {
                readURL(this);
                $('.img-responsive').remove();
            });

            $('.close').on('click', function() {
                $('#image').remove();
            });
        });
    </script>
    @endpush