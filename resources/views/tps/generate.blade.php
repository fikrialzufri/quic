@extends('template.app')

@section('content')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tambah {{$title}}</h3>
                </div>
                <!-- /.card-header -->
                <form action="{{ $action }}" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group ">
                            <label for="kelurahan">Kelurahan</label>
                            <select name="kelurahan_id" class="selected2 form-control" id="cmbkelurahan">
                                <option value="">--Pilih Kelurahan--</option>
                                <option value="all">Semua Kelurahan</option>
                                @foreach ($dataKelurahan as $kelurahan)
                                <option value="{{$kelurahan->id}}" {{old('kelurahan_id') == $kelurahan->id ? "selected" : ""}}>{{'Kelurahan : '.$kelurahan->nama.', Kecamatan : '.$kelurahan->nama_kecamatan.', Kabupaten : '.$kelurahan->nama_kabupaten}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('kelurahan_id'))
                            <span class="text-danger">
                                <strong id="textkelurahan_id">{{ $errors->first('kelurahan_id')}}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div>
                                <label for="jumlah" class=" form-control-label">Jumlah {{$title}}</label>
                            </div>
                            <div>
                                <input type="text" name="jumlah" placeholder="Jumlah {{$title}}" id="jumlah" class="form-control  {{$errors->has('jumlah') ? 'form-control is-invalid' : 'form-control'}}" value="{{old('jumlah')}}" required>
                            </div>
                            @if ($errors->has('jumlah'))
                            <span class="text-danger">
                                <strong id="textjumlah">{{ $errors->first('jumlah')}}</strong>
                            </span>
                            @endif
                        </div>

                    </div>

                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

    @stop

    @push('script')
    <script>
        $(function() {
            $("#jumlah").keypress(function() {
                $("#jumlah").removeClass("is-invalid");
                $("#textjumlah").html("");
            });
            $("#jumlah").inputFilter(function(value) {
                return /^\d*$/.test(value); // Allow digits only, using a RegExp
            });
            $('#cmbkabupaten').select2({
                placeholder: '--- Pilih Kabupaten---',
                width: '100%'
            });
            $('#cmbkecamatan').select2({
                placeholder: '--- Pilih Kecamatan---',
                width: '100%'
            });
            $('#cmbkelurahan').select2({
                placeholder: '--- Pilih Kelurahan---',
                width: '100%'
            });
        });
    </script>
    @endpush