<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Inbox extends Model
{
    use UsesUuid;
    // mengnonaktifkan incrementing
    public $incrementing = false;

    protected $table = "inbox";
    protected $fillable = [];
    protected $guarded = [];
    protected $appends = ['time'];

    public function getTimeAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
