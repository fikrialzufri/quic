<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use Auth;

class Paslon extends Model
{
    use UsesUuid;
    // mengnonaktifkan incrementing
    public $incrementing = false;

    protected $table = "paslon";
    protected $fillable = [];
    protected $guarded = [];
    protected $appends = ['suara'];

    public function perhitungan()
    {
        return $this->hasMany(Perhitungan::class);
    }

    public function getSuaraAttribute()
    {
        if ($this->perhitungan) {
            return $this->perhitungan->sum('jumlah');
        }
    }

    public function getSeluruhSuaraAttribute()
    {
        if ($this->perhitungan) {
            return $this->perhitungan->sum('jumlah');
        }
    }

    public function getSuaraSaksiAttribute()
    {
        if ($this->perhitungan) {
            return $this->perhitungan->where('saksi_id', Auth::user()->saksi_id)->sum('jumlah');
        }
    }

    public function getPresentasiAttribute()
    {

        if ($this->perhitungan) {
            return $this->perhitungan->where('saksi_id', Auth::user()->saksi_id)->sum('jumlah');
        }
    }
}
