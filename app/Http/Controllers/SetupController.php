<?php

namespace App\Http\Controllers;

use App\Models\Setup;
use Str;
use Illuminate\Http\Request;

class SetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title =  "Setup";
        $dataSetup = Setup::paginate(5);
        $route = 'setup';
        return view('setup.index', compact("title", "dataSetup", "route"));
    }

    /**
     * Show the form for creating a new resource.
     *p
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title =  "setup";
        $route = 'setup';
        $action = route('setup.store');

        return view('setup.create', compact(
            "title",
            "action"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
        ];

        $this->validate(request(), [
            'kode' => 'required|unique:paslon,kode',
            'pesan' => 'required',
            // 'nourut' => 'required|unique:paslon,nourut',

        ], $messages);
        $kode = $request->kode;
        $slug = Str::slug($kode, '-');

        $setup = new Setup;
        $setup->pesan =  $request->pesan;
        $setup->kode =  $slug;
        $setup->save();
        return redirect()->route('setup.index')->with('message', 'Setup berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setup  $setup
     * @return \Illuminate\Http\Response
     */
    public function show(Setup $setup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setup  $setup
     * @return \Illuminate\Http\Response
     */
    public function edit(Setup $setup)
    {
        $title =  "Setup";
        $route = 'setup';
        $action = route('setup.update', $setup->id);

        return view('setup.edit', compact(
            "title",
            "action",
            "setup"
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setup  $setup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setup $setup)
    {

        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
        ];

        $this->validate(request(), [
            'kode' => 'required|unique:paslon,kode,' . $setup->id,
            'pesan' => 'required',
            // 'nourut' => 'required|unique:paslon,nourut',

        ], $messages);
        $kode = $request->kode;
        $slug = Str::slug($kode, '-');

        $setup->pesan =  $request->pesan;
        $setup->kode =  $slug;
        $setup->save();
        return redirect()->route('setup.index')->with('message', 'Setup berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setup  $setup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setup $setup)
    {
        $setup->delete();

        return redirect()->route('setup.index')->with('message', 'Setup berhasil dihapus')->with('Class', 'Hapus');
    }
}
