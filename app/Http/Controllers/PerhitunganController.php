<?php

namespace App\Http\Controllers;

use App\Models\Perhitungan;
use App\Models\Saksi;
use App\Models\Paslon;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Models\Tps;
use Excel;
use Auth;

use Str;
use Storage;

use Intervention\Image\Facades\Image;

use App\Exports\ExcelPerhitungan;
use Illuminate\Http\Request;

class PerhitunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title =  "Perhitungan";
        $jumlah =  0;
        $kelurahan_id = [];
        $dataPaslon = Paslon::where('paslon', 'ya')->orderBy('nourut', 'ASC')->get();
        $dataNonPaslon = Paslon::where('paslon', 'tidak')->orderBy('nourut', 'ASC')->get()->pluck('id');
        $dataKelurahan = Kelurahan::orderBy('nama', 'ASC')->get();
        $dataKecamatan = Kecamatan::orderBy('nama', 'ASC')->get();
        $dataTps = Tps::orderBy('nama', 'ASC')->get();

        $paslon = request()->get('paslon') ?: "";
        $limit = request()->get('limit') ?: "";
        $kelurahan = request()->get('kelurahan') ?: "";
        $tps = request()->get('tps') ?: "";

        $route = 'perhitungan';


        $paslon_id = Paslon::where('paslon', 'ya')->orderBy('nourut', 'ASC')->get()->pluck('id');
        $dataPerhitungan = Perhitungan::whereIn('paslon_id', $paslon_id)->paginate(10);
        $dataPerhitunganNonPaslon = Perhitungan::whereIn('paslon_id', $dataNonPaslon)->paginate(10);

        if ($paslon == "all" || $paslon == "") {
            if ($kelurahan != "") {
                if ($kelurahan == "allkecamatan") {
                    $kecamatan_id = Kecamatan::all()->pluck('id');
                    $kelurahan_id = Kelurahan::whereIn('kecamatan_id', $kecamatan_id)->get()->pluck('id');
                    if ($tps != "") {

                        $checktps = Tps::where('nama', 'LIKE', '%' . $tps . '%')->whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                    } else {
                        $checktps = Tps::whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                    }
                    $checksaksi = Saksi::whereIn('tps_id', $checktps)->get();
                    if ($paslon_id) {
                        $dataPerhitungan = Perhitungan::whereIn('paslon_id', $paslon_id)->whereIn('saksi_id', $checksaksi->pluck('id'))->paginate(10);
                    } else {
                        $dataPerhitungan = Perhitungan::whereIn('saksi_id', $checksaksi)->paginate(10);
                    }
                } else {
                    // $kelurahan;
                    $checkkecamatan = Kecamatan::find($kelurahan);
                    if ($checkkecamatan) {
                        $kelurahan_id = Kelurahan::where('kecamatan_id', $kelurahan)->get()->pluck('id');;
                    } else {
                        $kelurahan_id = [$kelurahan];
                    }
                    if ($tps != "") {
                        $checktps = Tps::where('nama', 'LIKE', '%' . $tps . '%')->whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                    } else {

                        $checktps = Tps::whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                    }

                    $checksaksi = Saksi::whereIn('tps_id', $checktps)->get()->pluck('id');
                    $dataPerhitungan = Perhitungan::whereIn('paslon_id', $paslon_id)->whereIn('saksi_id', $checksaksi)->paginate(10);
                    $dataPerhitunganNonPaslon = Perhitungan::whereIn('paslon_id', $dataNonPaslon)->whereIn('saksi_id', $checksaksi)->paginate(10);
                }
            }
        } else {


            if ($kelurahan != "") {
                if ($kelurahan == "allkecamatan") {
                    $kelurahan_id = Kelurahan::all()->pluck('id');
                } else {

                    $checkkecamatan = Kecamatan::find($kelurahan);
                    if ($checkkecamatan) {
                        $kelurahan_id = Kelurahan::where('kecamatan_id', $kelurahan)->get()->pluck('id');;
                    } else {
                        $kelurahan_id = [$kelurahan];
                    }
                }
                if ($tps != "") {
                    $checktps = Tps::where('nama', 'LIKE', '%' . $tps . '%')->whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                } else {
                    $checktps = Tps::whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                }
            } else {

                $kelurahan_id = Kelurahan::all()->pluck('id');
                if ($tps != "") {
                    $checktps = Tps::where('nama', 'LIKE', '%' . $tps . '%')->whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                } else {
                    $checktps = Tps::whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                }
            }
            $checksaksi = Saksi::whereIn('tps_id', $checktps)->get()->pluck('id');

            $dataPerhitungan = Perhitungan::where('paslon_id', $paslon)->whereIn('saksi_id', $checksaksi)->paginate(10);

            $dataPerhitunganNonPaslon = Perhitungan::whereIn('paslon_id', $dataNonPaslon)->whereIn('saksi_id', $checksaksi)->paginate(10);
        }
        $jumlah = $dataPerhitungan->sum('jumlah');
        $jumlahNon = $dataPerhitunganNonPaslon->sum('jumlah');

        return view('perhitungan.index', compact(
            "title",
            "dataPerhitungan",
            "dataKelurahan",
            "dataKecamatan",
            "dataTps",
            "route",
            "tps",
            "paslon",
            "jumlah",
            "jumlahNon",
            "kelurahan",
            "dataPerhitunganNonPaslon",
            "dataPaslon"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title =  "Perhitungan";
        $route = 'perhitungan';
        $dataSaksi = Saksi::all();
        $dataPaslon = Paslon::orderBy('nourut', 'ASC')->get();
        $route = 'perhitungan';
        $action = route('perhitungan.store');

        return view('perhitungan.create', compact(
            "title",
            "action",
            "dataPaslon",
            "dataSaksi",

            "route"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
            'min' => ':attribute minimal 1',
        ];

        $this->validate(request(), [
            // 'paslon_id' => 'required|unique:perhitungan,paslon_id',
            // 'saksi_id' => 'required|unique:perhitungan,saksi_id',
            'jumlah' => 'required|max:13|min:1',

        ], $messages);
        $perhitungan = Perhitungan::where('paslon_id', $request->paslon_id)->where('saksi_id', $request->saksi_id)->first();
        if (!$perhitungan) {
            $perhitungan = new Perhitungan;
        }
        $perhitungan->paslon_id =  $request->paslon_id;
        $perhitungan->tanggal =  now();
        $perhitungan->jumlah =  $request->jumlah;
        $perhitungan->saksi_id =  $request->saksi_id;

        $perhitungan->save();

        return redirect()->route('perhitungan.index')->with('message', 'perhitungan berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Perhitungan  $perhitungan
     * @return \Illuminate\Http\Response
     */
    public function show(Perhitungan $perhitungan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Perhitungan  $perhitungan
     * @return \Illuminate\Http\Response
     */
    public function edit(Perhitungan $perhitungan)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Perhitungan  $perhitungan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Perhitungan $perhitungan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Perhitungan  $perhitungan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Perhitungan $perhitungan)
    {
        // Perhitungan::truncate();
        // return redirect()->route('perhitungan.index')->with('message', 'Perhitungan berhasil dihapus');
    }
    public function hapus()
    {
        Perhitungan::truncate();
        return redirect()->route('perhitungan.index')->with('message', 'Perhitungan berhasil dihapus');
    }

    public function hasil()
    {
        $paslon = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'ya');
        $nourut = [];
        $suara = [];
        $nama = [];
        $presentasicalon = [];
        $suaratidak = 0;

        $nonpaslon = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'tidak')->where('kode', 'LIKE', '%tidak%')->get()->pluck('id');

        $paslon_id = $paslon->get()->pluck('id');
        $perhitungan = Perhitungan::whereIn('paslon_id', $paslon_id)->get();

        $perhitungannonpaslon = Perhitungan::whereIn('paslon_id', $nonpaslon)->get();

        if ($perhitungan) {
            $suarasah = $perhitungan->sum('jumlah');
        }
        if ($perhitungannonpaslon) {
            $suaratidak = $perhitungannonpaslon->sum('jumlah');
        }

        $seluruh = $suarasah + $suaratidak;

        $persetase = ($seluruh / 187877) * 100;

        foreach ($paslon->get() as $key => $item) {
            $nourut[$key] = $item->nourut;
            $suara[$key] = $item->suara;
            $nama[$key] = $item->nama;
            if ($item->suara) {
                # code...
                $presentasicalon[$key] = round(($item->suara / $seluruh) * 100, 2);
            }
        }


        $data = [
            'nourut' => $nourut,
            'paslon' => $paslon->get(),
            'nama' => $nama,
            'suara' => $suara,
            'suaratidak' => $suaratidak,
            'suarasah' => $suarasah,
            'seluruh' => $seluruh,
            'presentasicalon' => $presentasicalon,
            'persetase' => round($persetase, 2),

        ];
        return response()->json($data);
    }
    public function hasilsuara()
    {
        $paslon = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'tidak')->where('kode', 'like', '%tidak%');
        $nourut = [];
        $suara = [];
        $nama = [];
        $jumlah = 0;
        foreach ($paslon->get() as $key => $item) {
            $nourut[$key] = $item->nourut;
            $suara[$key] = $item->suara;
            $nama[$key] = $item->nama;
        }

        $seluruh = Perhitungan::where('paslon_id', $paslon->get()->pluck('id'))->get();
        if ($seluruh) {
            $tidaksah = $seluruh->sum('jumlah');
        }
        $data = [
            'nourut' => $nourut,
            'paslon' => $paslon,
            'nama' => $nama,
            'suara' => $suara,
            'tidaksah' => $tidaksah,
        ];
        return response()->json($data);
    }

    public function excel()
    {
        $paslon = request()->get('paslon') ?: "";
        $kelurahan = request()->get('kelurahan') ?: "";
        $tps = request()->get('tps') ?: "";

        return Excel::download(new ExcelPerhitungan($paslon, $kelurahan, $tps), 'Export Perhitungan Suara.xlsx');
    }

    public function saksi(Request $request)
    {
        // return $request;
        $saksi_id =  Auth::user()->saksi_id;
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
            'max' => ':attribute maksimal 13',
            'min' => ':attribute minimal 10',
        ];

        $this->validate(request(), [
            'foto' => 'mimes:jpeg,bmp,png,jpg,pdf|max:10240'
        ], $messages);

        foreach ($request->hasil as $key => $value) {

            $perhitungan = Perhitungan::where('paslon_id', $request->id[$key])->where('saksi_id', $saksi_id)->first();
            if (!$perhitungan) {
                $perhitungan = new Perhitungan;
            }
            $perhitungan->paslon_id =  $request->id[$key];
            $perhitungan->tanggal =  now();
            $perhitungan->jumlah =  $value;
            $perhitungan->saksi_id =  $saksi_id;

            $perhitungan->save();
        }

        $saksi = Saksi::find($saksi_id);
        $slug = Str::slug($saksi->nohp, '-');
        $foto = $request->foto;

        if (isset($foto)) {

            $nama_gambar = $slug . 'c1.' . $foto->getClientOriginalExtension();
            if (Storage::disk('public')->exists('saksi/' . $saksi->fotocsatu)) {
                Storage::disk('public')->delete('saksi/' . $saksi->fotocsatu);
            }
            $request->file('foto')->move(
                base_path() . '/public/storage/saksi/',
                $nama_gambar
            );

            $saksi->fotocsatu = $nama_gambar;
        }
        $saksi->save();

        return redirect()->route('home')->with('message', 'Perhitungan berhasil ditambah')->with('swall', 'oke');
    }
}
