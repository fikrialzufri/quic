<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Paslon;
use App\Models\Saksi;
use App\Models\Perhitungan;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title =  "Dashboard";
        $paslon = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'ya')->get();
        $paslonall = Paslon::orderBy('nourut', 'ASC')->get();
        $nourut = [];
        $suara = [];
        foreach ($paslon as $key => $item) {
            $nourut[$key] = $item->nourut;
            $suara[$key] = $item->suara;
        }

        $paslon2 = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'ya');
        $nourut = [];
        $suara = [];
        $nama = [];
        $suaratidak = 0;

        $nonpaslon = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'tidak')->where('kode', 'LIKE', '%tidak%')->get()->pluck('id');

        $paslon_id = $paslon2->get()->pluck('id');
        $perhitungan = Perhitungan::whereIn('paslon_id', $paslon_id)->get();

        $perhitungannonpaslon = Perhitungan::whereIn('paslon_id', $nonpaslon)->get();

        if ($perhitungan) {
            $suarasah = $perhitungan->sum('jumlah');
        }
        if ($perhitungannonpaslon) {
            $suaratidak = $perhitungannonpaslon->sum('jumlah');
        }

        $seluruh = $suarasah + $suaratidak;

        $saksi = Saksi::find(Auth::user()->saksi_id);

        $setting = Setting::first();

        return view('home.index', compact(
            'title',
            'suara',
            'nourut',
            "saksi",
            "seluruh",
            "setting",
            "paslon",
            "paslonall"
        ));
    }
}
