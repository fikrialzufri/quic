<?php

namespace App\Http\Controllers;

use App\Models\Saksi;
use App\Models\User;
use App\Models\Tps;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Models\Perhitungan;


use Str;
use Storage;
use Excel;

use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

class SaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title =  "Saksi";
        $nama = request()->get('nama') ?: "";
        $nohp = request()->get('hp') ?: "";
        $status = request()->get('status') ?: "";

        $dataSaksi = Saksi::orderby('nama', 'ASC')->paginate(15);
        $route = 'saksi';

        $dataKelurahan = Kelurahan::orderBy('nama', 'ASC')->get();
        $dataKecamatan = Kecamatan::orderBy('nama', 'ASC')->get();
        $kelurahan = request()->get('kelurahan') ?: "";

        if ($kelurahan != "") {
            if ($kelurahan == "allkecamatan") {
                $kecamatan_id = Kecamatan::all()->pluck('id');
                $kelurahan_id = Kelurahan::whereIn('kecamatan_id', $kecamatan_id)->get()->pluck('id');
                $checktps = Tps::whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                if ($status != "") {
                    if ($status == "sudah") {
                        $dataPerhitungan = Perhitungan::get()->pluck('saksi_id');
                        $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->whereIn('id', $dataPerhitungan)->orderby('nama', 'ASC')->paginate(15);
                    }
                    if ($status == "belum") {
                        $dataPerhitungan = Perhitungan::get()->pluck('saksi_id');
                        $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->whereNotIn('id', $dataPerhitungan)->orderby('nama', 'ASC')->paginate(15);
                    }
                    if ($status == "semua") {

                        $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->paginate(15);
                    }
                } else {

                    $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->paginate(15);
                }
            } else {
                // $kelurahan;
                $checkkecamatan = Kecamatan::find($kelurahan);
                if ($checkkecamatan) {
                    $kelurahan_id = Kelurahan::where('kecamatan_id', $kelurahan)->get()->pluck('id');;
                } else {
                    $kelurahan_id = [$kelurahan];
                }
                $checktps = Tps::whereIn('kelurahan_id', $kelurahan_id)->get()->pluck('id');
                if ($status != "") {
                    if ($status == "sudah") {
                        $dataPerhitungan = Perhitungan::get()->pluck('saksi_id');
                        $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->whereIn('id', $dataPerhitungan)->orderby('nama', 'ASC')->paginate(15);
                    }
                    if ($status == "belum") {
                        $dataPerhitungan = Perhitungan::get()->pluck('saksi_id');
                        $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->whereNotIn('id', $dataPerhitungan)->orderby('nama', 'ASC')->paginate(15);
                    }
                    if ($status == "semua") {

                        $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->paginate(15);
                    }
                } else {
                    $dataSaksi = Saksi::whereIn('tps_id', $checktps)->where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->paginate(15);
                }
            }
        } else {
            if ($status != "") {
                if ($status == "sudah") {
                    $dataPerhitungan = Perhitungan::get()->pluck('saksi_id');
                    $dataSaksi = Saksi::where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->whereIn('id', $dataPerhitungan)->orderby('nama', 'ASC')->paginate(15);
                }
                if ($status == "belum") {
                    $dataPerhitungan = Perhitungan::get()->pluck('saksi_id');
                    $dataSaksi = Saksi::where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->whereNotIn('id', $dataPerhitungan)->orderby('nama', 'ASC')->paginate(15);
                }
                if ($status == "semua") {

                    $dataSaksi = Saksi::where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->paginate(15);
                }
            } else {

                $dataSaksi = Saksi::where('nohp', 'LIKE', '%' . $nohp . '%')->where('nama', 'LIKE', '%' . $nama . '%')->paginate(15);
            }
        }
        return view('saksi.index', compact(
            "title",
            "dataSaksi",
            "nama",
            "nohp",
            "status",
            "kelurahan",
            "dataKelurahan",
            "dataKecamatan",
            "route"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title =  "Saksi";
        $route = 'saksi';
        $dataTps = Tps::orderBy('nama', 'ASC')->get();
        $action = route('saksi.store');

        return view('saksi.create', compact(
            "title",
            "action",
            "dataTps"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
            'max' => ':attribute maksimal 13',
            'min' => ':attribute minimal 10',
        ];

        $this->validate(request(), [
            'nik' => 'required|unique:saksi',
            'nama' => 'required',
            'alamat' => 'required',
            'nohp' => 'required|unique:saksi|max:13|min:10',
            'tps_id' => 'required|unique:saksi',

        ], $messages);

        $nama = $request->nama;
        $slug = Str::slug($nama, '-');
        $foto = $request->foto;

        $saksi = new Saksi;
        $saksi->nik =  $request->nik;
        $saksi->nama =  $nama;
        $saksi->nohp =  $request->nohp;
        $saksi->alamat =  $request->alamat;
        $saksi->tps_id =  $request->tps_id;

        if ($foto) {
            $this->validate(request(), [
                'foto' => 'mimes:jpeg,bmp,png,jpg'
            ], $messages);

            $nama_gambar = $slug . '.' . $foto->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('saksi')) {
                Storage::disk('public')->makeDirectory('saksi');
            }

            $path = public_path('storage/saksi/' . $nama_gambar);

            $gambar_original = Image::make($foto)->resize(720, 720)->save($path);
            Storage::disk('public')->put('saksi/' . $nama_gambar, $gambar_original);

            if (!Storage::disk('public')->exists('saksi/thumbnail')) {
                Storage::disk('public')->makeDirectory('saksi/thumbnail');
            }
            $thumbnail = Image::make($foto)->resize(360, 360)->save($path);
            Storage::disk('public')->put('saksi/thumbnail/' . $nama_gambar, $thumbnail);
        } else {
            $nama_gambar = NULL;
        }
        $saksi->foto = $nama_gambar;

        $saksi->save();
        $userexsis = User::where('name', 'LIKE', '%' . $nama . '%')->first();
        if ($userexsis) {
            $nama = $nama . " saksi";
        }
        $user = new User;
        $user->name = $nama;
        $user->username = $request->nohp;
        $user->email = str_replace(' ', '', $nama) . '@gmail.com';
        $user->password = bcrypt($request->nohp);
        $user->rule = 2;
        $user->saksi_id = $saksi->id;
        $user->save();
        return redirect()->route('saksi.index')->with('message', 'Saksi berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Saksi  $saksi
     * @return \Illuminate\Http\Response
     */
    public function show(Saksi $saksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Saksi  $saksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Saksi $saksi)
    {
        $title =  "Saksi";
        $route = 'saksi';
        $dataTps = Tps::orderBy('nama', 'ASC')->get();
        $action = route('saksi.update', $saksi->id);

        return view('saksi.edit', compact(
            "title",
            "action",
            "dataTps",
            "saksi"
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Saksi  $saksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Saksi $saksi)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
            'max' => ':attribute maksimal 13',
            'min' => ':attribute minimal 10',
        ];

        $this->validate(request(), [
            'nik' => 'required|unique:saksi,nik,' . $saksi->id,
            'nama' => 'required',
            'alamat' => 'required',
            'nohp' => 'required|max:13|min:10|unique:saksi,tps_id,' . $saksi->id,
            'tps_id' => 'required|unique:saksi,tps_id,' . $saksi->id,

        ], $messages);

        $nama = $request->nama;
        $slug = Str::slug($nama, '-');
        $foto = $request->foto;

        $saksi->nik =  $request->nik;
        $saksi->nama =  $nama;
        $saksi->nohp =  $request->nohp;
        $saksi->alamat =  $request->alamat;
        $saksi->tps_id =  $request->tps_id;

        if (isset($foto)) {
            $this->validate(request(), [
                'foto' => 'mimes:jpeg,bmp,png,jpg'
            ], $messages);
            $nama_gambar = $slug . '.' . $foto->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('saksi')) {
                Storage::disk('public')->makeDirectory('saksi');
            }

            $path = public_path('storage/saksi/' . $nama_gambar);

            if (Storage::disk('public')->exists('saksi/' . $saksi->foto)) {
                Storage::disk('public')->delete('saksi/' . $saksi->foto);
            }

            $gambar_original = Image::make($foto)->resize(720, 720)->save($path);
            Storage::disk('public')->put('saksi/' . $nama_gambar, $gambar_original);

            if (!Storage::disk('public')->exists('saksi/thumbnail')) {
                Storage::disk('public')->makeDirectory('saksi/thumbnail');
            }

            if (Storage::disk('public')->exists('saksi/thumbnail/' . $saksi->foto)) {
                Storage::disk('public')->delete('saksi/thumbnail/' . $saksi->foto);
            }
            $thumbnail = Image::make($foto)->resize(720, 720)->save($path);
            Storage::disk('public')->put('saksi/thumbnail/' . $nama_gambar, $thumbnail);
            $saksi->foto = $nama_gambar;
        }

        $saksi->save();

        $user = User::where('saksi_id', $saksi->id)->first();
        if (!$user) {
            $user = new User();
        }
        $userexsis = User::where('name', 'LIKE', '%' . $nama . '%')->first();
        if ($userexsis) {
            $nama = $nama . " saksi";
        }
        $user->name = $nama;
        $user->username = $request->nohp;
        $user->email = str_replace(' ', '', $nama) . $request->nohp . '@gmail.com';
        $user->password = bcrypt($request->nohp);
        $user->rule = 2;
        $user->saksi_id = $saksi->id;
        $user->save();
        return redirect()->route('saksi.index')->with('message', 'Saksi berhasil ditambah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Saksi  $saksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Saksi $saksi)
    {
        $saksi->delete();
        $user = User::where('saksi_id', $saksi->id)->first();
        if ($user) {
            $user->delete();
        }

        return redirect()->route('saksi.index')->with('message', 'Saksi berhasil dihapus')->with('Class', 'Hapus');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        $title =  "Saksi";
        $route = 'saksi';
        $dataTps = Tps::orderBy('nama', 'ASC')->get();
        $action = route('saksi.upload');

        return view('saksi.upload', compact(
            "title",
            "action",
            "dataTps"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama',
        ];

        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        $file = $request->file('file');

        $ext =  request()->file('file')->getClientOriginalExtension();
        $excel =  request()->file('file')->getRealPath() . '.' . $ext;
        $saksi = Excel::toArray('', request()->file('file'), null, null);
        try {
            $dataSaksi = [];
            $dataTps = [];
            $dataNohp = [];
            $dataKelurahan = [];
            $dataNik = [];
            $dataNama = [];
            $dataAlamat = [];
            foreach ($saksi[0] as $k => $val) {

                $dataSaksi[$k] = $val;
            }
            foreach ($dataSaksi as $index => $item) {
                if ($index > 2) {
                    # code...
                    $dataTps[$index] = Saksi::where('nohp', $item[5])->count();
                    $dataNohp[$index] = "";
                    if ($dataTps[$index] == 0) {
                        $dataKelurahan[$index] = Kelurahan::where('nama', $item[2])->first();
                        if ($dataKelurahan[$index]) {
                            $dataTps[$index] = Tps::where('nama', 'LIKE', '%' . $item[1] . "%")->where('kelurahan_id',  $dataKelurahan[$index]->id)->first();
                            if ($dataTps[$index]) {

                                $dataNik[$index] = $item[3];
                                $dataNama[$index] = $item[4];
                                $dataNohp[$index] = $item[5];
                                $dataAlamat[$index] = $item[6];

                                $saksi = new Saksi;
                                $saksi->nik =  $dataNik[$index];
                                $saksi->nama =  $dataNama[$index];
                                $saksi->nohp =  $dataNohp[$index];
                                $saksi->alamat =  $dataAlamat[$index];
                                $saksi->tps_id =  $dataTps[$index]->id;
                                $saksi->save();

                                $user = User::where('saksi_id', $saksi->id)->first();
                                if (!$user) {
                                    $user = new User();
                                }
                                $user->name = $dataNama[$index];
                                $user->username = $dataNohp[$index];
                                $user->email = str_replace(' ', '', $dataNama[$index]) . $dataNohp[$index] . '@gmail.com';
                                $user->password = bcrypt($dataNohp[$index]);
                                $user->rule = 2;
                                $user->saksi_id = $saksi->id;
                                $user->save();
                            }
                        }
                    }
                }
            }
            return redirect()->route('saksi.index')->with('message', 'Saksi diupload ditambah');
        } catch (\Throwable $th) {
            return redirect()->route('saksi.index')->with('message', 'Format excel anda salah, mohon ulang dengan data yang benar');
        }
    }
}
