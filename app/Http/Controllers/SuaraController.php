<?php

namespace App\Http\Controllers;

use App\Models\Paslon;
use Str;
use Illuminate\Http\Request;

class SuaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title =  "Suara";
        $dataPaslon = Paslon::orderBy('nourut', 'ASC')->where('paslon', 'tidak')->paginate(5);
        $route = 'suara';
        return view('suara.index', compact("title", "dataPaslon", "route"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title =  "Suara";
        $route = 'suara';
        $action = route('suara.store');

        return view('suara.create', compact("title", "action", "route"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
        ];

        $this->validate(request(), [
            'kode' => 'required|unique:paslon,kode',
            'nama' => 'required',
            // 'nourut' => 'required|unique:paslon,nourut',

        ], $messages);
        $kode = $request->kode;
        $slug = Str::slug($kode, '-');

        $suara = new Paslon;
        $suara->nama =  $request->nama;
        $suara->kode =  $slug;
        $suara->nourut =  $slug;
        $suara->paslon =  'tidak';
        $suara->save();
        return redirect()->route('suara.index')->with('message', 'Suara berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paslon = Paslon::find($id);
        $title =  "suara " . $paslon->nama;
        $action = route('suara.update', $paslon->id);
        return view('suara.edit', compact('action', 'title', 'paslon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $suara = Paslon::find($id);

        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
        ];

        $this->validate(request(), [
            'kode' => 'required|unique:paslon,kode,' . $suara->id,
            'nama' => 'required',
            // 'nourut' => 'required|unique:paslon,nourut',

        ], $messages);
        $kode = $request->kode;
        $slug = Str::slug($kode, '-');


        $suara->nama =  $request->nama;
        $suara->kode =  $slug;
        $suara->nourut =  $slug;
        $suara->paslon =  'tidak';
        $suara->save();
        return redirect()->route('suara.index')->with('message', 'Suara berhasil ditambah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suara = Paslon::find($id);
        $suara->delete();

        return redirect()->route('suara.index')->with('message', 'Suara berhasil dihapus')->with('Class', 'Hapus');
    }
}
