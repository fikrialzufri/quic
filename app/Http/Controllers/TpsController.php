<?php

namespace App\Http\Controllers;

use App\Models\Tps;
use App\Models\Kelurahan;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use Illuminate\Http\Request;

class TpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title =  "Tps";
        $dataTps = Tps::orderBy('kelurahan_id', 'ASC')->orderBy('nama', 'ASC')->paginate(10);
        $route = 'tps';

        $limit = request()->get('limit') ?: "";
        $kelurahan = request()->get('kelurahan') ?: "";
        $nama = request()->get('nama') ?: "";

        $dataKelurahan = Kelurahan::orderBy('nama', 'ASC')->get();
        $dataKecamatan = Kecamatan::orderBy('nama', 'ASC')->get();

        if ($nama == "all" || $nama == "") {
            if ($kelurahan != "") {
                if ($kelurahan == "allkecamatan") {
                    $kecamatan_id = Kecamatan::all()->pluck('id');
                    $kelurahan_id = Kelurahan::whereIn('kecamatan_id', $kecamatan_id)->get()->pluck('id');
                    $dataTps = Tps::whereIn('kelurahan_id', $kelurahan_id)->orderBy('nama', 'ASC')->paginate(15);
                } else {
                    $checkkelurahan = Kelurahan::find($kelurahan);
                    if (!$checkkelurahan) {
                        $kecamatan_id = Kecamatan::find($kelurahan);
                        $kelurahan_id = Kelurahan::whereIn('kecamatan_id', $kecamatan_id)->get()->pluck('id');
                        $dataTps = Tps::where('kelurahan_id', $kelurahan_id)->orderBy('nama', 'ASC')->paginate(15);
                    } else {
                        $kelurahan_id = $checkkelurahan->id;
                        $dataTps = Tps::where('kelurahan_id', $kelurahan_id)->orderBy('nama', 'ASC')->paginate(15);
                    }
                }
            }
        } else {
            # code...
            if ($kelurahan != "") {
                if ($kelurahan == "allkecamatan") {
                    $kecamatan_id = Kecamatan::all()->pluck('id');
                    $kelurahan_id = Kelurahan::whereIn('kecamatan_id', $kecamatan_id)->get()->pluck('id');
                    $dataTps = Tps::where('nama', "like", "%" . $nama . "%")->orderBy('nama', 'ASC')->whereIn('kelurahan_id', $kelurahan_id)->paginate(15);
                } else {
                    $checkkelurahan = Kelurahan::find($kelurahan);
                    if (!$checkkelurahan) {
                        $kecamatan_id = Kecamatan::find($kelurahan);
                        $kelurahan_id = Kelurahan::whereIn('kecamatan_id', $kecamatan_id)->get()->pluck('id');
                        $dataTps = Tps::where('nama', "like", "%" . $nama . "%")->where('kelurahan_id', $kelurahan_id)->orderBy('nama', 'ASC')->paginate(15);
                    } else {
                        $kelurahan_id = $checkkelurahan->id;
                        $dataTps = Tps::where('nama', "like", "%" . $nama . "%")->where('kelurahan_id', $kelurahan_id)->orderBy('nama', 'ASC')->paginate(15);
                    }
                }
            } else {

                $dataTps = Tps::where('nama', "like", "%" . $nama . "%")->orderBy('kelurahan_id', 'ASC')->orderBy('nama', 'ASC')->paginate(10);
            }
        }


        return view('tps.index', compact(
            "title",
            "dataTps",
            "nama",
            "dataKelurahan",
            "kelurahan",
            "dataKecamatan",
            "route"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title =  "TPS";
        $route = 'tps';
        $dataKabupaten = Kabupaten::all();
        $dataKecamatan = Kecamatan::all();
        $dataKelurahan = Kelurahan::all();
        $action = route('tps.store');

        return view('tps.create', compact(
            "title",
            "action",
            "dataKabupaten",
            "dataKecamatan",
            "dataKelurahan"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
        ];

        $this->validate(request(), [
            'nama' => 'required|unique:tps,nama,' . $request->nama . ',id,kelurahan_id,' . $request->kelurahan_id,
            'kelurahan_id' => 'required|min:1',
        ], $messages);

        $tps = new Tps;
        $tps->nama =  $request->nama;
        $tps->kelurahan_id =  $request->kelurahan_id;
        $tps->save();
        return redirect()->route('tps.index')->with('message', 'TPS berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tps  $tps
     * @return \Illuminate\Http\Response
     */
    public function show(Tps $tps)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tps  $tps
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tps = Tps::find($id);
        $title =  "TPS " . $tps->nama;
        $dataKabupaten = Kabupaten::all();
        $dataKecamatan = Kecamatan::all();
        $dataKelurahan = Kelurahan::all();
        $action = route('tps.update', $tps->id);
        return view('tps.edit', compact(
            'action',
            'title',
            'tps',
            'dataKabupaten',
            "dataKecamatan",
            "dataKelurahan"
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tps  $tps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tps = Tps::find($id);
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'date' => ':attribute harus menggunakan tanggal yang benar',
            'date_format' => ':attribute harus menggunakan tanggal yang benar',
            'max' => ':attribute maksimal 30',
        ];

        $this->validate(request(), [
            'nama' => 'required|unique:tps,nama,' . $id . ',id,kelurahan_id,' . $request->kelurahan_id,
            'kelurahan_id' => 'required|min:1',
        ], $messages);


        $tps->nama = $request->nama;
        $tps->kelurahan_id = $request->kelurahan_id;
        $tps->save();

        return redirect()->route('tps.index')->with('message', 'Berhasil Mengubah Data TPS')->with('Class', 'Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tps  $tps
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tps = Tps::find($id);
        $tps->delete();

        return redirect()->route('tps.index')->with('message', 'TPS berhasil dihapus')->with('Class', 'Berhasil');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generate()
    {
        $title =  "Generate TPS";
        $route = 'tps';
        $dataKabupaten = Kabupaten::all();
        $dataKecamatan = Kecamatan::all();
        $dataKelurahan = Kelurahan::all();
        $action = route('tps.generateapplay');

        return view('tps.generate', compact(
            "title",
            "action",
            "dataKabupaten",
            "dataKecamatan",
            "dataKelurahan"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateapplay(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'date' => ':attribute harus menggunakan tanggal yang benar',
            'date_format' => ':attribute harus menggunakan tanggal yang benar',
            'max' => ':attribute maksimal 30',
        ];

        $this->validate(request(), [
            'kelurahan_id' => 'required',
            'jumlah' => 'required|min:1',
        ], $messages);

        $kelurahan_id = $request->kelurahan_id;
        $namatps = [];
        for ($i = 0; $i < $request->jumlah; $i++) {
            # code...
            $namatps[$i] = "TPS" . str_pad($i + 1, 2, "0", STR_PAD_LEFT);
        }
        foreach ($namatps as $index => $item) {

            if ($kelurahan_id == "all") {
                $kelurahan = Kelurahan::all();

                foreach ($kelurahan as $key => $value) {
                    $tps = Tps::where('nama', $value)->where('kelurahan_id', $kelurahan_id)->first();
                    if (!$tps) {
                        $tps = new Tps;
                    }
                    $tps->nama =  $item;
                    $tps->kelurahan_id =  $value->id;
                    $tps->save();
                }
            } else {
                $tps = Tps::where('nama', $item)->where('kelurahan_id', $kelurahan_id)->first();
                if (!$tps) {
                    $tps = new Tps;
                }
                $tps->nama =  $item;
                $tps->kelurahan_id =  $kelurahan_id;
                $tps->save();
            }
        }

        return redirect()->route('tps.index')->with('message', 'Berhasil Generate TPS ' . $request->jumlah)->with('Class', 'Berhasil');
    }
}
