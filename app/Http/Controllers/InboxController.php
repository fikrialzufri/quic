<?php

namespace App\Http\Controllers;

use App\Models\Inbox;
use App\Models\Saksi;
use App\Models\Paslon;
use App\Models\Perhitungan;


use Illuminate\Http\Request;

class InboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title =  "SMS";
        $jenis = request()->get('jenis') ?: "";

        if ($jenis == "all" || $jenis == "") {
            $dataInbox = Inbox::orderby('created_at', 'DESC')->paginate(20);
        } else {
            $dataInbox = Inbox::orderby('created_at', 'DESC')->where('jenis', $jenis)->paginate(20);
        }
        $route = 'inbox';
        return view('inbox.index', compact(
            "title",
            "dataInbox",
            "jenis",
            "route"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title =  "Kirim SMS";
        $route = 'inbox';
        $dataSaksi = Saksi::all();
        $action = route('inbox.store');

        return view('inbox.create', compact("title", "action", "dataSaksi"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $semua =  $request->semua;
        $saksi = $request->saksi;

        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute tidak boleh sama dengan data yang terdahulu',
            'same' => 'Password dan konfirmasi password harus sama',
        ];

        $this->validate(request(), [
            'pesan' => 'required',
        ], $messages);
        if (!$semua) {
            $this->validate(request(), [
                'pesan' => 'required',
                'saksi' => 'required',
            ], $messages);
        }
        $pesan =  $request->pesan;
        $idsms =  "2942369";
        $modem =  "BORNEO_081319455963";
        $auth =  "fa4ca952c31dbc991be0499551c5b8c4";
        $pesan == "gagal";
        $jenis = "";

        $hasil = [];

        if ($semua) {
            $dataSaksi = Saksi::all();

            foreach ($dataSaksi as $index => $item) {
                $inbox = new Inbox();
                try {
                    $message = str_replace(' ', '%20', $pesan);
                    $url = "http://secure.gosmsgateway.com/api/send.php?username=borneoci&mobile=" . $item->nohp . "&message=" . $message . "&password=gosms8834";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    if ($result == "1701") {
                        $jenis = "send";
                    } else {
                        $jenis = "outbox";
                    }
                    // return $response;

                    $hasil[$index] = $result;
                } catch (\Throwable $th) {
                    //throw $th;
                    $jenis = "outbox";
                }
                $inbox->id_sms =  $idsms;
                $inbox->sender =  $item->nohp;
                $inbox->content = $pesan;
                $inbox->modem = $modem;
                $inbox->auth = $auth;
                $inbox->jenis = $jenis;
                $inbox->tanggal = now();
                $inbox->save();
            }
        }
        if ($saksi) {
            $dataSaksi = Saksi::whereIn('id', $saksi)->get();
            foreach ($dataSaksi as $index => $item) {
                $inbox = new Inbox();
                $result = "";
                try {
                    $message = str_replace(' ', '%20', $pesan);
                    $url = "http://secure.gosmsgateway.com/api/send.php?username=borneoci&mobile=" . $item->nohp . "&message=" . $message . "&password=gosms8834";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    if ($result == "1701") {
                        $jenis = "send";
                    } else {
                        $jenis = "outbox";
                    }
                    $hasil[$index] = $result;

                    sleep(1);
                } catch (\Throwable $th) {
                    $jenis = "outbox";
                }
                $inbox->id_sms =  $idsms;
                $inbox->sender =  $item->nohp;
                $inbox->content = $pesan;
                $inbox->modem = $modem;
                $inbox->auth = $auth;
                $inbox->tanggal = now();
                $inbox->jenis = $jenis;
                $inbox->save();
            }
            // return $inbox;
        }
        $jumlah = count($hasil);
        // return redirect()->route('inbox.index')->with(
        //     'message',
        //     'Sms anda berhasil, jumlah yang berhasil terkirim adalah ' . $jumlah
        // )->with('Class', 'berhasil');

        return response()->json($jumlah);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function show(Inbox $inbox)
    {
        $title =  "Inbox";
        $inbox->status = 'baca';
        $action = route('inbox.update', $inbox->id);
        $inbox->save();
        return view('inbox.detail', compact("title", "inbox", 'action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function edit(Inbox $inbox)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inbox $inbox)
    {
        try {
            $message = str_replace(' ', '%20', $inbox->content);
            $url = "http://secure.gosmsgateway.com/api/send.php?username=borneoci&mobile=" . $inbox->sender . "&message=" . $message . "&password=gosms8834";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: " . fakeip(), "X-Client-IP: " . fakeip(), "Client-IP: " . fakeip(), "HTTP_X_FORWARDED_FOR: " . fakeip(), "X-Forwarded-For: " . fakeip()));  

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_PROXY, "127.0.0.1:8888"); 
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result == "1701") {
                $jenis = "send";
            } else {
                $jenis = "outbox";
            }
            // return $response;
            $inbox->jenis = $jenis;
            $inbox->save();

            return redirect()->route('inbox.index')->with('message', 'Sms anda berhasil')->with('Class', 'berhasil');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->route('inbox.index')->with('message', 'Sms anda gagal mohon hubungi provider')->with('Class', 'gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inbox $inbox)
    {
        //
    }

    /**
     * data inbox receive from storage.
     *
     * @param  \App\Inbox  $inbox
     * @return \Illuminate\Http\Response
     */
    public function receive(Request $request)
    {
        // return $request;
        $result = "";
        $jenis = "";

        try {
            $inbox = new Inbox;
            $inbox->id_sms =  $request->idsms;
            $inbox->sender =  $request->sender;
            $inbox->content = $request->content;
            $inbox->modem = $request->modem;
            $inbox->auth = $request->auth;
            $inbox->tanggal = $request->datetime;
            $inbox->jenis = "inbox";
            $inbox->save();

            $saksi = Saksi::where('nohp', $request->sender)->first();
            $content = $request->content;
            $kode = '';
            $suara = '';
            $paslon_id = '';
            if ($saksi) {
                if ($content) {
                    $arrayconten = (explode(" ", $content));
                    foreach ($arrayconten as $key => $value) {
                        if ($key == 0) {
                            # code...
                            $kode = $value;
                        }
                        if ($key == 1) {
                            $suara = $value;
                        }
                    }
                    if ($kode != "") {
                        $paslon = Paslon::where('kode', $kode)->first();
                        if ($paslon) {
                            $paslon_id = $paslon->id;

                            if ($suara > 0) {
                                try {
                                    //code...
                                    $perhitungan = Perhitungan::where('paslon_id', $paslon_id)->where('saksi_id', $saksi->id)->first();
                                    if (!$perhitungan) {
                                        $perhitungan = new Perhitungan;
                                    }
                                    $perhitungan->paslon_id =  $paslon_id;
                                    $perhitungan->tanggal =  now();
                                    $perhitungan->jumlah =  $suara;
                                    $perhitungan->saksi_id =  $saksi->id;

                                    $perhitungan->save();
                                    $messages = "Terimakasih atas poling anda";
                                } catch (\Throwable $th) {
                                    $messages = "gagal database eror";
                                }
                            } else {
                                $messages = "Harap masukkan suara dengan angka";
                            }
                        } else {
                            $messages = "Keyword anda salah";
                        }
                    } else {
                        $messages = "Keyword anda salah";
                    }
                } else {
                    $messages = "SMS ada kurang tepat";
                }
            } else {
                $messages = "Anda bukan saksi";
            }

            $send = new Inbox;



            try {
                $message = str_replace(' ', '%20', $messages);
                $url = "http://secure.gosmsgateway.com/api/send.php?username=borneoci&mobile=" . $request->sender . "&message=" . $message . "&password=gosms8834";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: " . fakeip(), "X-Client-IP: " . fakeip(), "Client-IP: " . fakeip(), "HTTP_X_FORWARDED_FOR: " . fakeip(), "X-Forwarded-For: " . fakeip()));  

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt($ch, CURLOPT_PROXY, "127.0.0.1:8888"); 
                $result = curl_exec($ch);
                curl_close($ch);
                if ($result == "1701") {
                    $jenis = "send";
                } else {
                    $jenis = "outbox";
                }
                // return $response;
            } catch (\Throwable $th) {
                //throw $th;
            }
            $send->id_sms =  $request->idsms;
            $send->sender =  $request->sender;
            $send->content = $messages;
            $send->modem = $request->modem;
            $send->auth = $request->auth;
            $send->tanggal = $request->datetime;
            $send->jenis = $jenis;
            $send->save();


            $data = [
                "status" => "succes",
                "data" => $inbox,
                "messages" => $messages,
                "result" => $result,
                "jenis" => $jenis,
            ];
            //code...
        } catch (\Throwable $th) {
            //throw $th;
            $data = [
                "status" => "gagal",
                "data" => $request,
                "messages" => $messages,
                "result" => $result,
                "jenis" => $jenis

            ];
        }


        return response()->json($data);
    }

    public function unread()
    {
        $dataInbox = Inbox::where('status', 'tidak')->where('jenis', 'inbox');

        $data = [
            'jumlah' => $dataInbox->count(),
            'data' => $dataInbox->orderBy('created_at', 'DESC')->skip(0)->take(4)->get(),
        ];

        return response()->json($data);
    }
}
