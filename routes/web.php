<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index');

    Route::group(['middleware' => ['auth', 'admin']], function () {
        Route::resource('user', 'UserController');
        Route::resource('kabupaten', 'KabupatenController');
        Route::resource('kecamatan', 'KecamatanController');
        Route::resource('kelurahan', 'KelurahanController');
        Route::resource('tps', 'TpsController');
        Route::resource('paslon', 'PaslonController');
        Route::resource('suara', 'SuaraController');
        Route::resource('saksi', 'SaksiController');
        Route::resource('perhitungan', 'PerhitunganController');
        Route::resource('setup', 'SetupController');
        Route::resource('setting', 'SettingController');

        Route::get('/datakecamatan', 'KecamatanController@data');

        Route::get('/formuploadkelurahan', 'KelurahanController@form')->name('kelurahan.formupload');
        Route::post('/uploadkeluarahan', 'KelurahanController@upload')->name('kelurahan.upload');

        Route::get('/formuploadsaksi', 'SaksiController@form')->name('saksi.formupload');
        Route::post('/uploadsaksi', 'SaksiController@upload')->name('saksi.upload');

        Route::get('/generate', 'TpsController@generate')->name('tps.generate');
        Route::post('/generateapplay', 'TpsController@generateapplay')->name('tps.generateapplay');
        Route::get('/excelperhitungan', 'PerhitunganController@excel')->name('perhitungan.excel');
        Route::post('/hapus', 'PerhitunganController@hapus')->name('perhitungan.hapus');

        Route::get('/kecamatandetai', 'KecamatanController@detail')->name('kecamatan.detail');
        Route::get('/kelurahandetai', 'KelurahanController@detail')->name('kelurahan.detail');

        Route::resource('inbox', 'InboxController');
    });



    Route::get('/unread', 'InboxController@unread')->name('inbox.unread');

    Route::get('/hasil', 'PerhitunganController@hasil')->name('perhitungan.hasil');

    Route::get('/hasilsuara', 'PerhitunganController@hasilsuara')->name('perhitungan.hasilsuara');
    Route::post('/hasilsaksi', 'PerhitunganController@saksi')->name('perhitungan.saksi');
});


Route::get('/receive', 'InboxController@receive');
